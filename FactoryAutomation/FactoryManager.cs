﻿using PlantPotFactoryApi;

namespace FactoryAutomation
{
    internal class FactoryManager
    {
        private IPlantPotFactory factory;

        public FactoryManager()
        {
            factory = PlantPotFactoryCreator.CreateFactory();

            factory.StockPurchaseArrived += Factory_StockPurchaseArrived;
            factory.PlantPotStateChanged += Factory_PlantPotStateChanged;
            factory.OrderCreated += Factory_OrderCreated;
            factory.OrderCanceled += Factory_OrderCanceled;


            UI.PrintEmphasized("Purchase initial stock.");

            // Buy some stock up front
            factory.CreateClayPurchaseRequest(24);

            // Buy some of each of the glazes
            for (int glaze = 0; glaze < (int)GlazeColor.MAX; glaze++)
            {
                factory.CreateGlazePurchaseRequest((GlazeColor)glaze, 16);
            }
        }

        public void RunDay()
        {
            UI.PrintEmphasized("Day: {0}, Cash: {1}", factory.Day, factory.Cash);

            factory.RunDay();          

            // Do additional tasks here  
        }

        private void Factory_OrderCanceled(FactoryOrderID orderID)
        {
            UI.PrintError("{0} has been canceled.", orderID);

            // Do order managment tasks here
        }

        private void Factory_OrderCreated(FactoryOrderID orderID, int numberOfPots, GlazeColor topGlaze, GlazeColor bottomGlaze)
        {
            UI.PrintEmphasized("Created {0}, Number of pots: {1}, Top glaze: {2}, Bottom glaze: {3}.", orderID, numberOfPots, topGlaze, bottomGlaze);

            // Do order managment tasks here
        }

        private void Factory_PlantPotStateChanged(PlantPotID plantPotID, PlantPotState state)
        {
            UI.Print(" - {0}: {1}.", plantPotID, state);

            // Do plant pot managment tasks here
        }

        private void Factory_StockPurchaseArrived(PurchaseRequestID purchaseRequestID)
        {
            UI.Print("Stock purchase arrived, {0}.", purchaseRequestID);

            // Do glaze and clay stock managment tasks here
        }
    }
}