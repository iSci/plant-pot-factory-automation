﻿namespace FactoryAutomation
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            UI.PrintAssemblyName();

            UI.PrintEmphasized("Welcome to Factory Simulator 2016");
            UI.PrintPrompt("Press any key to begin.");

            UI.PrintBlankLine();

            FactoryManager factoryManager = new FactoryManager();

            while (true)
            {
                factoryManager.RunDay();

                UI.PrintPrompt("Press any key for next day.");
                UI.PrintBlankLine();
            }
        }
    }
}