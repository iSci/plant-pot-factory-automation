﻿using System;
using System.Reflection;

namespace FactoryAutomation
{
    internal static class UI
    {
        private const ConsoleColor emphasis = ConsoleColor.White;
        private const ConsoleColor error = ConsoleColor.Red;
        private const ConsoleColor errorDetail = ConsoleColor.DarkRed;
        private const ConsoleColor normal = ConsoleColor.DarkGray;
        private const ConsoleColor prompt = ConsoleColor.DarkCyan;

        private readonly static object syncLock = new object();

        public static void Print(string format, params object[] args)
        {
            PrintWithColor(normal, format, args);
        }

        public static void PrintAssemblyName()
        {
            AssemblyName assemblyName = typeof(Program).Assembly.GetName();

            PrintEmphasized("{0} v{1}.{2}", assemblyName.Name, assemblyName.Version.Major, assemblyName.Version.Minor);

            PrintBlankLine();
        }

        public static void PrintBlankLine()
        {
            Console.WriteLine();
        }

        public static void PrintEmphasized(string format, params object[] args)
        {
            PrintWithColor(emphasis, format, args);
        }

        public static void PrintError(string format, params object[] args)
        {
            PrintWithColor(error, format, args);
        }

        public static void PrintException(string message, Exception ex)
        {
            PrintWithColor(error, "{0}", message);
            PrintWithColor(errorDetail, "{0}", ex.ToString());
        }

        public static ConsoleKeyInfo PrintPrompt(string format, params object[] args)
        {
            PrintWithColor(prompt, format, args);

            ConsoleKeyInfo key = Console.ReadKey();

            PrintBlankLine();

            return key;
        }

        private static void PrintWithColor(ConsoleColor color, string format, params object[] args)
        {
            ConsoleColor backupColor = Console.ForegroundColor;

            try
            {
                Console.ForegroundColor = color;

                Console.WriteLine(string.Format(format, args));
            }
            finally
            {
                Console.ForegroundColor = backupColor;
            }
        }
    }
}