﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Factory Automation")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("WE MAKE PLANT POTS INC")]
[assembly: AssemblyProduct("Factory Automation")]
[assembly: AssemblyCopyright("Copyright © WE MAKE PLANT POTS 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("892504f8-9013-4d09-be35-f4a8b9a4fcc0")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]