# Plant Pot Factory Automation #

### WTF am I supposed to do? ###

The aim of this application is to automate the production and dispatching of plant pots. 
The factory has a convenient API (PlantPotFactoryApi.dll) so you can automate the factory from the luxury of your desk. 

* Manage the quantities of clay and glaze in stock.
* Enqueue the creation and glazing of pots. 
* Enqueue batches of pots for firing in the kiln. 
* Manage active orders and complete them with finished pots.
* Turn a profit. 

 
### Materials: ###

* Clay costs £3 per unit. It takes 2 days for clay stock to arrive from our supplier. 
* Glaze costs £10 per unit. It takes 7 days for glaze stock to arrive from our supplier. 


### Plant pots are: ###

* Made out of clay (1 unit of clay per pot).
* Are glazed in 2 parts, Top and Bottom (1 unit of glaze per part).
* Must be fired in the kiln before they can be sold.
* Our pot making machine can only make 3 pots a day. 
* We sell pots at £34 per unit. 
* A maximum of 48 plant pots of any state can me kept on the premisis at any one time.


### The kiln: ###

* Can fire a maximum of 8 pots in a single batch. 
* Can only fire one batch at a time. 
* Takes 1 day to complete a batch. 
* The cost of firing a batch of pots of any size in the kiln is £23.



### Orders: ###

* Orders will be of a single style of plant pot. e.g. 3 pots, red top glaze, blue, bottom glaze. 
* Orders are limited to a maximum of 8 pots. 
* Customers must allow 7 days, after that they have the option to cancel their order. 
* Some glazes and styles are more popular than others. 


### Expectations

We expect you to make more than £1000 profit over the first 365 days. 

# GO MAKE POTS